import Kommander.MasterKommander;

import java.io.File;
import java.util.Scanner;

public class Main {
    public static void main (String args[]) {
        Scanner leer = new Scanner(System.in);
        String key;
        String arc;
        System.out.println("Ingrese la URL del archivo donde se encuentra el archivo de las palabras clave");
        key=leer.nextLine();

        if ( ! new File(key).isFile() ) {
            System.out.println("Ha ingresado un archivo no valido");
            return;
        }

        System.out.println("Ingrese la URL del archivo donde desea buscar las palabras clave");
        arc=leer.nextLine();

        if ( ! new File(arc).isFile() ) {
            System.out.println("Ha ingresado un archivo no valido");
            return;
        }

        MasterKommander masterKommander = new MasterKommander();
        masterKommander.process(arc,key);
    }
}
