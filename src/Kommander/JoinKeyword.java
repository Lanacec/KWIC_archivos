package Kommander;


import java.util.ArrayList;
import java.util.Hashtable;

public class JoinKeyword {

    private Context context;
    public JoinKeyword(Context context){
        this.context= context;

    }


    public void process (){
        context.noRepeat=new ArrayList<>();

        Hashtable<String, Boolean> visitado = new Hashtable<>();

        for (int i=0;i<context.indices.size();i++){
            String keyword = context.indices.get(i).keyword;

            if ( ! visitado.containsKey(keyword) ) {
                ArrayList<Integer> nor = new ArrayList<>();
                nor.add(context.indices.get(i).page);
                for (int j=i+1;j<context.indices.size();j++){
                    if(context.indices.get(i).keyword.equals(context.indices.get(j).keyword)){
                        nor.add(context.indices.get(j).page);

                    }
                }

                Context.GenericPair par = new Context.GenericPair(keyword,nor);
                context.noRepeat.add(par);

                visitado.put(keyword, true);
            }
        }


    }

}



