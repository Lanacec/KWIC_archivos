package Kommander;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Scanner;


// MasterKomander
// KeywordInput -> Lee archivo de keyword y los almacena en la variable keywords.txt de context
// Input -> Lee archivo donde se buscará y lo almacena en la variable chars
//

public class Input {
    private Context context;

    public Input(Context context) {
        this.context = context;
    }

    public void run(String fileName, String keywordsFile) {
        context.keywords= new ArrayList();

        try {
            Scanner read = new Scanner(new File(keywordsFile));
            while (read.hasNext()){
                String line = read.nextLine().trim();

                if ( ! line.isEmpty() ) {
                    context.keywords.add(line);
                }
            }


            byte[] content = Files.readAllBytes(Paths.get(fileName));

            context.chars = new byte[content.length];
            for(int i = 0; i < content.length; i++) {
                context.chars[i] = content[i];
            }

        } catch (IOException e) {
        }
    }
}

