package Kommander;

public class MasterKommander {
    ///private Alphabetizer alphabetizer;
   // private CircularShift circularShift;
    private Input input;
    private Output output;
    private Context context;
    private FindWord findword;
    private JoinKeyword joinkeyword;

    public MasterKommander() {
        context = new Context();

  //      alphabetizer = new Alphabetizer(context);
    //    circularShift = new CircularShift(context);
        input = new Input(context);
        findword = new FindWord(context);
        output = new Output(context);
        joinkeyword= new JoinKeyword(context);

    }

    public void process(String fileName, String keywordsFile) {
        input.run(fileName, keywordsFile);
        findword.process();
        joinkeyword.process();
        output.run();
        //circularShift.run();
        //alphabetizer.run();
       // output.run();
    }
}