package Kommander;


import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.ByteArrayInputStream;
import java.util.ArrayList;

public class FindWord {
    public Context context;


    public FindWord(Context context){
        this.context=context;
    }

    public void process(){
        ByteArrayInputStream bais = new ByteArrayInputStream(context.chars);
        context.indices= new ArrayList<>();
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();

            Document doc = builder.parse(bais);

            NodeList pages = doc.getElementsByTagName("page");
            for(int i = 0; i < pages.getLength(); i++) {
                Node page = pages.item(i);

                String pageContent = page.getTextContent();
                int index;
                String keyword;
                //busca palabra en la pagina i
                for (int j=0; j < context.keywords.size(); j++){
                    index = pageContent.indexOf(context.keywords.get(j));
                    keyword = context.keywords.get(j);
                    if( index != -1 ) {

                        Context.Pair par = new Context.Pair(keyword,i);
                        context.indices.add(par);
                    }
                }

            }

        } catch (Exception e) {}
    }
}
