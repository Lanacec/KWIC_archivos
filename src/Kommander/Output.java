package Kommander;

public class Output {
    Context context;

    public Output(Context context) {
        this.context = context;
    }

    public void run() {
        for (int i=0;i<context.noRepeat.size();i++){

            Context.GenericPair pair = context.noRepeat.get(i);
            System.out.print(pair.key + ": ");
            for(int j = 0; j < pair.nor.size(); j++) {
                if ( j == pair.nor.size() - 1 ) {
                    System.out.print(pair.nor.get(j));
                } else {
                    System.out.print(pair.nor.get(j) + ", ");
                }
            }

            System.out.println();
        }
    }
}
