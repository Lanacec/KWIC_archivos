package Kommander;

import java.util.ArrayList;

public class Context {

    static class Pair {
        String keyword;
        int page;

        public Pair(String keyword, int page) {
            this.keyword = keyword;
            this.page = page;
        }
    }

    static class GenericPair{
        String key;
        ArrayList<Integer> nor;

        public GenericPair(String key, ArrayList<Integer> nor){
            this.key=key;
            this.nor=nor;
        }
    }

    public byte[] chars;
    public ArrayList<String> keywords;
    public ArrayList<Pair> indices;
    public ArrayList<GenericPair> noRepeat;
}
